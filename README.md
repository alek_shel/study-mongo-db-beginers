# study-mongo-db-beginers

Изучение курса "Уроки MongoDB для начинающих" от Гоши Дударя
https://www.youtube.com/watch?v=xwr3azx0Gf0&list=PL0lO_mIqDDFXcxN3fRjc-EOWZLqW8dLVV 

# Установка базы данных на Мак и Windows

MongoDB - это система, которая предоставляется с открытым исходным кодом, имеет
простую и понятную реализацию и отлично подходит для NodeJS и JS в частности.

Является NoSQL базой данных и это значит, что данные в ней записываются не в привычном
формате таблиц и значений, а в формате объектов.

mongo --version
mongod [initandlisten]

Подключение после установки, на windows - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
1. Создать папку data/db на диске C
2. Запуск MongoDB, cdm -> "C:\Program Files\MongoDB\Server\4.4\bin\mongod.exe" --dbpath="c:\data\db"
3. Подключение к MongoDB, cmd -> "C:\Program Files\MongoDB\Server\4.4\bin\mongo.exe"

На macOS
1. Создать папку data/db в главном диске
2. В термина запустить -> mongod
3. В терминале запустить -> mongo --host localhost:27017

# Создание базы данных

Для использования базы данных
use [dbName]

Коллекция - это, некий объект, который содержит определённую информацию.
Например, users - это коллекция содержащая информацию о пользователях.

Создание коллекции 1й способ -> db.createCollection("users")
Создание коллекции 2й способ -> если вы хотите поместить запись в ту
коллекцию, которой не существует, то коллекция создастся автоматически.

В реляционных базах данных, например MySQL, мы создаём таблицы с чётким
набором полей, которые будут присутствовать в каждой записи, а в NoSQL
базах данных можно создавать более эластичные коллекции, в которой
каждая запись (объект), может иметь разное кол-во полей.

Удаление коллекции db.articles.drop()

Если удалить все коллекции, то вместе с последней удалится и сама БД.

# Добавление данных в коллекцию

Любая запись в коллекции представляет из себя JS объект.

Добавление одной новой записи
db.[collactionName].insertOne({...})

Ответ:
"acknowledged" : true,
"insertedIds" : ObjectId("5f48c4b0da027538aed7d32e")

Добавление нескольких записей
db.[collactionName].insertMany([{...}, {...}])

Ответ:
"acknowledged" : true,
"insertedIds" : [
  ObjectId("5f48c4b0da027538aed7d32e"),
  ObjectId("5f48c4b0da027538aed7d32f")
]



В качестве полей записи можно передовать, как простые типы значений:
строка, число, логический тип, так и объекты, и массивы.

! Если была добавлена новая запись но она не отображается в коллекции,
значит не была указана БД с которой мы работаем.

Если не указать _id, он создаётся автоматически.

# Выборка данных из коллекции

Вывод всех записей из коллекции db.users.find()
Вывод 2х записей db.users.find().limit(2)
Убрать поле _id -> db.users.find({}, { _id: 0 }).limit(2)

Функцию сортировки можно прописать сразу после find или после limit
В сортировке sort, значение - 1 -> по возрастанию, -1 -> по убыванию:
db.users.find({}, { _id: 0 }).sort({age: 1})

=== Фильтры ===

Вывод всех пользователей с возрастом равным 22
db.users.find({
  age: 22
}, { _id: 0 })

Логика для оператора ИЛИ
db.users.find({
  $or: [{age: 22}, {email: 'bob@test.ru'}]
}, { _id: 0 })

$lt (letter then) -> меньше
db.users.find({
  $or: [{age: {$lt: 38}}, {email: 'bob@test.ru'}]
}, { _id: 0 }).sort({age: 1})

$gt (greater then) -> больше
db.users.find({
  $or: [{age: {$gt: 38}}, {email: 'bob@test.ru'}]
}, { _id: 0 }).sort({age: 1})

$in -> содержится в
db.users.find({
  name: {$in: ["Jack", "John", "Боб"]}
}, { _id: 0 })

$nin (not in) -> не содержится в
db.users.find({
  name: {$nin: ["Jack", "John", "Боб"]}
}, { _id: 0 })

$exists -> наличие данных в поле
db.users.find({
  child: {$exists: true}
}, { _id: 0 })

Как только прописываем поле в выборке, даже без значения,
в выборке уже будут учавствовать объекты с этим полем.

Проверяем на наличие поля favColors и двух элементов в нём
db.users.find({
  favColors: {$size: 2}
}, { _id: 0 })

Отслеживание конкретного элемента в массиве на значение
db.users.find({
  "favColors.1": "Красный"
}, { _id: 0 })

$elemMatch -> элемент соответствует
db.users.find({
  favColors: {$elemMatch: {$lte: 'a'}}
}, { _id: 0 })