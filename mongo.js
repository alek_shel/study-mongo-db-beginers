/**
 * Добавление данных в коллекцию
 */

db.users.insertOne(
  {
    "name": "John",
    "email": "test@mail.ru",
    "age": 23,
    "hasCar": true,
    "favColors": ["Зеленый", "Красный", "Черный"],
    "child": {
      "name": "Jack",
      "surname": "Charley",
      "age": 5
    }
  }
)

db.users.insertOne(
  {
    _id: 2,
    "name": "Jack",
    "email": "admin@mail.ru",
    "age": 43,
    "hasCar": false,
    "favColors": ["Красный", "Белый"],
    "child": {
      "name": "Jack",
      "surname": "Charley",
      "age": 5
    }
  }
)

db.users.insertOne(
  {
    "name": "Jack",
    "email": "admin@mail.ru",
    "age": 43,
    "hasCar": false,
    "favColors": ["Красный", "Белый"],
    "password": "dsfsdf"
  }
)

db.users.insertOne(
  {
    "name": "George",
    "email": "admin@mail.ru",
    "age": 22,
    "hasCar": false,
    "birthday": new Date('1996-11-27')
  }
)

db.users.insertMany([
  {
    "name": "Боб",
    "email": "admin@mail.ru",
    "age": 22,
    "hasCar": false,
    "birthday": new Date('1996-11-27')
  },
  {
    "name": "Василий",
    "email": "vasya@mail.ru",
    "age": 22,
    "hasCar": false,
    "birthday": new Date('1996-11-27')
  },
])

/**
 * Выборка данных из коллекции
 */

db.users.find().limit(2)
db.users.find({}, { _id: 0 }).limit(2)
db.users.find({}, { _id: 0 }).sort({age: 1})
db.users.find({}, { _id: 0 }).sort({age: 1, email: -1})

db.users.find({
  age: 22, email: '...'
}, { _id: 0 })

db.users.find({
  $or: [{age: 22}, {email: 'bob@test.ru'}]
}, { _id: 0 })

db.users.find({
  $or: [{age: {$lt: 38}}, {email: 'bob@test.ru'}]
}, { _id: 0 }).sort({age: 1})

db.users.find({
  name: {$in: ["Jack", "John", "Боб"]}
}, { _id: 0 })

db.users.find({
  child: {$exists: true}
}, { _id: 0 })

db.users.find({
  favColors: {$size: 2}
}, { _id: 0 })

db.users.find({
  "favColors.1": "Красный"
}, { _id: 0 })

db.users.find({
  favColors: {$elemMatch: {$lte: 'a'}}
}, { _id: 0 })